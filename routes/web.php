<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('welcome');
});

Auth::routes();


Route::get('/welcome', 'Web\PageController@blog')->name('welcome');
Route::get('/home', 'HomeController@index')->name('home');


Route::get('/project/{slug}', 'Web\PageController@project')->name('project');
Route::get('/referance/{slug}', 'Web\PageController@referance')->name('referance');
Route::get('/service/{slug}', 'Web\PageController@service')->name('service');
Route::get('/slider/{slug}', 'Web\PageController@slider')->name('slider');
Route::get('/post/{slug}', 'Web\PageController@post')->name('post');
Route::get('/category/{slug}', 'Web\PageController@category')->name('category');
Route::get('/tag/{slug}', 'Web\PageController@tag')->name('tag');


Route::resource('tags','Admin\TagController');
Route::resource('categories','Admin\CategoryController');
Route::resource('posts','Admin\PostController');
Route::resource('projects','Admin\ProjectController');
Route::resource('sliders','Admin\SliderController');
Route::resource('aboutuses','Admin\AboutusController');
Route::resource('contacts','Admin\ContactController');
Route::resource('referanslar','Admin\ReferanceController');
Route::resource('bartermedias','Admin\MedyaBarterController');
Route::resource('services','Admin\ServiceController');
Route::resource('settings','Admin\SettingController');
Route::resource('contactmessages','Admin\ContactMessageController');


Route::name('web.pages.')->group(function () {
    Route::get('referances', 'Web\ReferanceController@referance')->name('referances');
    Route::get('about-us', 'Web\AboutUsController@index')->name('about-us');
    Route::get('barter-takas', 'Web\BarterTakasController@index')->name('barter-takas');
    Route::get('contact', 'Web\ContactController@index')->name('contact');
});

Route::resource('homepages','Admin\HomePageController');
Route::resource('seoabouts','Admin\SeoAboutController');
Route::resource('seoreferances','Admin\SeoReferanceController');
Route::resource('seocontacts','Admin\SeoContactController');
Route::resource('seoprojects','Admin\SeoProjectController');
Route::resource('seobarters','Admin\SeoBarterController');


Route::namespace('Auth')->group(function () {
    Route::get('logout', 'LoginController@logout');
});