{{ Form::hidden('user_id', auth()->user()->id) }}

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('name', 'Proje Adı : ',['class' => ' col-form-label']) }}
            {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
        </div>

        <div class="form-group">
            {{ Form::label('slug', 'URL : ',['class' => ' col-form-label']) }}
            {{ Form::text('slug', null, ['class' => 'form-control', 'id' => 'slug']) }}
        </div>

        <div class="form-group">
            {{ Form::label('desciription', 'Açıklama : ',['class' => ' col-form-label']) }}
            {{ Form::textarea('desciription', null, ['class' => 'form-control', 'id' => 'desciription']) }}
        </div>
        <div class="form-group">
            {{ Form::label('image', 'Resim : ',['class' => ' col-form-label']) }}
            {{ Form::file('image') }}
        </div>

        <div class="form-group">
			<label>Etiketler:</label>
			<br/>
			<input data-role="tagsinput" type="text" name="tags" >
			@if ($errors->has('tags'))
                <span class="text-danger">{{ $errors->first('tags') }}</span>
            @endif
		</div>

        <div class="form-group">
            {{ Form::submit('Ekle', ['class' => 'btn btn-primary btn-wide col-md-12 ']) }}
        </div>
    </div>
    <div class="col-md-6">

    </div>
</div>

