{{ Form::hidden('user_id', auth()->user()->id) }}
<div class="row">
	<div class="col-md-6">
    <div class="form-group">
    {{ Form::label('type', 'Tipi') }}
    {!! Form::select('type', array('0' => 'Resim', '1' => 'Video'), '0',['class' => 'form-control', 'onchange' => 'showDiv()', 'id' => 'type']); !!}
</div>

<div class="form-group" >
    {{ Form::label('name', 'Slider Adı') }}
    {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
</div>

<div class="form-group" id="link">
    {{ Form::label('link', 'Youtube Link') }}
    {{ Form::text('link', null, ['class' => 'form-control', 'id' => 'link']) }}
    {{ Form::label('link', '** Video için Youtube linki yapıştırmanız yeterli.') }} 
</div>

<div class="form-group">
    {{ Form::label('slug', 'URL') }}
    {{ Form::text('slug', null, ['class' => 'form-control', 'id' => 'slug']) }}
</div>
<div class="form-group" id="resim">
    {{ Form::label('image', 'Resim') }}
    {{ Form::file('image') }}
</div>

<div class="form-group">
{{ Form::submit('Ekle', ['class' => 'btn btn-primary btn-wide col-md-12 ']) }}
</div>
    </div>
    <div class="col-md-6">
    </div>
</div>



