{{ Form::hidden('user_id', auth()->user()->id) }}

<div class="row">
    <div class="col-md-6">

    <div class="form-group">
            {{ Form::label('name', 'İsim: ') }}
            {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'disabled']) }}
        </div>

        <div class="form-group">
            {{ Form::label('email', 'E-Mail : ') }}
            {{ Form::text('email', null, ['class' => 'form-control', 'id' => 'email','disabled']) }}
        </div>
        <div class="form-group">
            {{ Form::label('message', 'Telefon 2 : ') }}
            {{ Form::textarea('message', null, ['class' => 'form-control', 'id' => 'message','disabled']) }}
        </div>

        <div class="form-group">
            {{ Form::submit('Ekle', ['class' => 'btn btn-primary btn-wide col-md-12 ']) }}
        </div>
    </div>

    <div class="col-md-6">
       
       
    </div>
</div>




<!-- <div class="form-group">
    {{ Form::label('image', 'Resim : ',['class' => ' col-form-label']) }}
    {{ Form::file('image') }}
</div> -->



