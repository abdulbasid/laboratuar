@extends('layouts.outside')

@section('content')
    <!-- Page Header Section  -->
	<section id="site-page-header" class="site-page-header blog-header polygon-bg">
		<div class="overlay-effects box-pattern index-1"></div>
		<div class="container">
			<div class="page-header-content display-table-middle">
				<div class="inside-content vertical-middle">
					<h1 class="page-title text-center highlighted" style="text-shadow: 1px 1px  #000;">{{ $service->title }}</h1>
				</div>
			</div><!--  /.page-header-content -->
			<div class="site-breadcrumb pull-right">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html">Anasayfa</a></li>
					<li class="breadcrumb-item"><a href="#">Hizmetlerimiz</a></li>
					<li class="breadcrumb-item active">{{ $service->title }}</li>
				</ol>
			</div>
		</div><!--  /.container -->
	</section><!-- /#page-header  -->
	<!-- Page Header Section End -->

	


	<!-- About Section -->
	<section style="background:#FFF" id="about-us" class="about-page section-padding">
		<div class="container">
			<div class="row">

				<div class="col-md-6">
					<div class="about-description">
						<h3 class="about-title bm30">{{ $service->title }}</h3>
						<p>
                            <!-- <span class="dropcap"><img src="{{ asset('web/images/ikonolcak.png') }}" alt="M"></span> -->
                            {{ $service->desciription }}
                        </p>
                  
				  </div>


				</div><!-- /.col-md-6 -->

				<div class="col-md-6">
				
					<article class="post type-post">
						<div class="post-thumbnail">
							<div id="postImgSlider" class="carousel slide" data-ride="carousel">
								<ol class="carousel-indicators">
									<li data-target="#postImgSlider" data-slide-to="0" class="active"></li>
									<li data-target="#postImgSlider" data-slide-to="1" class=""></li>
									
								</ol>
								<div class="carousel-inner"> 
								
									<div class="item active">
                                    @if($service->file)   
                                        <img alt="{{ $service->name }}" src="{{ $service->file }}">
                                    @endif
									</div>
									<!-- <div class="item">
										<img src="../images/Medya_Planlama_Laboratuar_Emre_Bora_2.jpg" alt="Medya_Planlama_Laboratuar_Emre_Bora">
									</div> -->
                               
								</div><!-- /.carousel-inner -->
								<a class="slide-nav left" href="#postImgSlider" data-slide="prev"><i class="fa fa-long-arrow-left"></i></a>
								<a class="slide-nav right" href="#postImgSlider" data-slide="next"><i class="fa fa-long-arrow-right"></i></a>
							</div><!-- /#postImgSlider -->
						</div>
					</article>
				
				</div>

				<aside class="widget widget_tag_cloud">
                <div class="stripe-full">
                    <h4 class="widget-title stripe-over stripe-blue"><span>Kelime</span> Havuzu</h4>
                </div>

				<div class="tagcloud">								
                    @if($seoservicetags->count())
                        @foreach($seoservices as $key => $seoservice)
                        @foreach($seoservice->tags as $tag) <a href="#">#{{$tag->name}}</a>@endforeach
                        @endforeach
                    @endif
                </div><!--  /.tagcloud -->

            </aside>
			  <div class="container">
              <div class="about-description">
				       
				        <p> <strong> </strong><br>
				        
                          
                       
			          </div>
				  <div class="row">
				    <!-- /.col-md-6 -->
				    <!-- /.col-md-6 -->
                    
			      </div>
				  <!-- /.row -->
			  </div>
				<!-- /.col-md-6 -->
				
			</div><!-- /.row -->
		</div><!-- /.container -->

	</section><!-- /#about  -->
    <!-- About Section End -->
    
    @endsection