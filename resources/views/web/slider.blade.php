<section id="slider" class="slider-section">
    <div id="home-focuz-main-slider" class="master-slider-parent focuz-main-slider ms-parent-id-2"  >
        <div id="focuz-main-slider" class="master-slider ms-skin-default" >    
            <div class="ms-slide slide1" data-delay="9" data-fill-mode="fill" >
            @foreach($sliders as $slider)
                <video width="100%" height="100%"  data-autopause="false" data-mute="true" data-loop="true" data-fill-mode="fill">
                    <source src="{{ $slider->file }}" type="video/mp4"/>
                    <!-- <source src="../video/1.webm" type="video/webm"/>
                    <source src="../video/1.ogv" type="video/ogg"/> -->
                </video>
            @endforeach
            </div>
        </div>
    </div>
 </section>
