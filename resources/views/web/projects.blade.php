<section id="works" class="clearfix"> 
    <div class="container">
        <div class="section-padding">
            <div class="title-container text-center">
                <h2 class="striped-title">
                    BARTER <span>İŞ</span> ORTAKLARIMIZ
                    <span class="stripe-t-left"></span>
                    
                    <span class="stripe-t-right"></span>
                </h2>
            </div>

            <div id="portfolio-item" class="portfolio-item v2 clearfix" style="text-align:center;">
            @foreach($projects as $pro)
                <div class="col-md-3 col-sm-4 col-xs-6 xs-full bm30 ">
                        
                <!-- <a href="{{ route('project', $pro->slug) }}"> -->
                        @if($pro->file)
                        <img alt="{{ $pro->name }}" src="{{ $pro->file }}" />
                        @endif
                <!-- </a> -->
                </div>
                @endforeach

                <div class="clearfix"></div>
            </div>
        </div>
    </div> 
</section>