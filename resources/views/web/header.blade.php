<header id="header"> 
    <div id="top-menu-container">
        <div id="header-wrap"> 
            <div class="container clearfix"> 
                <div id="primary-menu-trigger">
                    <i class="fa fa-reorder"></i>
                </div>

                <div id="logo">
                    <a href="{{ route('welcome') }}" class="standard-logo">

                    @foreach($settings as $setting)
                        @if($setting->favicon)
                            <img alt="{{ $setting->logoalt }}" src="{{ $setting->logo }}"></a>
                        @endif
                    @endforeach
                    
                    </a>
                </div>

                <nav id="primary-menu">

                    <ul class="main-menu">
                        <li class="current has-sub-menu"><a href="{{ route('welcome') }}"><div>ANASAYFA</div></a></li>

                        <li class="has-sub-menu"><a href="{{ route('web.pages.about-us') }}">HAKKIMIZDA</a></li>

                        <li class="has-sub-menu"><a href="#"><div>HİZMETLERİMİZ</div></a>
                            <ul class="sub-menu">
                            @foreach($services as $service)

                                <li><a href="{{ route('service', $service->slug) }}">{{ $service->title }}</a></li>

                            @endforeach
                            </ul>
                        </li>

                        <li class="has-sub-menu"><a href="{{ route('web.pages.barter-takas') }}">MEDYA BARTER (TAKAS)</a></li>

                        <li><a href="{{ route('web.pages.contact') }}"><div>BİZE ULAŞIN</div></a> </li>
                    </ul>
                
                </nav>
            </div>
        </div> 
    </div>
</header>