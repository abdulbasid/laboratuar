<section id="works" class="clearfix"> 
    <div class="container">
        <div class="section-padding">
            <div class="title-container text-center">
                <h2 class="striped-title">
                    REFERANS<span>LARIMIZ</span>
                    <span class="stripe-t-left"></span>
                    
                    <span class="stripe-t-right"></span>
                </h2>
            </div>
            
            <div id="portfolio-item" class="portfolio-item v2 clearfix" style="text-align:center;">

                
            @foreach($referances as $ref)
                <div class="col-md-3 col-sm-4 col-xs-6 xs-full bm30 ">
                        
                        <!-- <a href="{{ route('referance', $ref->slug) }}"> -->
                                @if($ref->file)
                                <img alt="{{ $ref->name }}" src="{{ $ref->file }}" />
                                @endif
                        <!-- </a> -->
                </div>
                @endforeach

               
                <div class="clearfix"></div>
                
            </div>
                
        </div>
    </div>
</section>