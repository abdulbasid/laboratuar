<div class="site-footer"> 
    <div id="footer-top-area" class="footer-top-common">
        <div class="container">
            <div class="footer-site-logo pull-left">
                <img src="{{ asset('web/images/logolabo.png')}}" alt="Laboratuar Mind Craft">
            </div>  
            <div class="social-icons pull-right">
                <ul>
                @foreach($contacts as $contact)

                    @if (is_null($contact->facebook) )
                    <li>
                    </li>
                    @else
                    <li><a href="{{ $contact->facebook }}" target="_blank"> <div class="icon-content fa-facebook">Facebook</div></a></li> 
                    @endif

                    @if (is_null($contact->linkdin) )
                    <li>
                    </li>
                    @else
                    <li><a href="{{ $contact->linkedin }}" target="_blank"><div class="icon-content fa-linkedin">Linkedin</div></a></li>  
                    @endif

                    @if (is_null($contact->instgram) )
                    <li>
                    </li>
                    @else
                    <li><a href="{{ $contact->instagram }}" target="_blank"><div class="icon-content fa-instagram">Instagram</div></a></li>
                    @endif

                    @if (is_null($contact->youtube) )
                    <li>
                    </li>
                    @else
                    <li>
                    <a href="{{ $contact->youtube }}" target="_blank"><div class="icon-content fa-youtube">YouTube Kanalı</div></a></li>
                    @endif

                    @if (is_null($contact->twitter) )
                    <li>
                    </li>
                    @else
                    <li><a href="{{ $contact->twitter }}" target="_blank"><div class="icon-content fa-twitter">Twitter</div></a></li>
                    @endif


                 @endforeach  
                   
                </ul>
            </div>
        </div>
    </div>
</div>