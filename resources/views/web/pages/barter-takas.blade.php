@extends('layouts.outside')

@section('content')
<section id="site-page-header" class="site-page-header blog-header polygon-bg" >
	<div class="overlay-effects box-pattern index-1"></div>
	<div class="container">
		<div class="page-header-content display-table-middle">
			<div class="inside-content vertical-middle">
				<h1 class="page-title text-center highlighted" style="text-shadow: 1px 1px  #000;">MEDYA BARTER (TAKAS)</h1>
			</div>
		</div>
		<div class="site-breadcrumb pull-right">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ route('welcome') }}">Anasayfa</a></li>
				
				<li class="breadcrumb-item active">Medya Barter (Takas)</li>
			</ol>
		</div>
	</div>
</section>

<section style="background:#FFF" id="about-us" class="about-page section-padding">
	<div class="container">
		<div class="row">
		@foreach($bartermedias as $bartermedia)
			<div class="col-md-6">
				<div class="about-description">
					<h3 class="about-title bm30"><span>{{ $bartermedia->title }}</h3>
					<p>
						<span class="dropcap">
							<img src="{{ asset('web/images/ikonolcak.png')}}" alt="M">
						</span>
						{{ $bartermedia->desciription }}
					</p>
			

				</div>
			</div>
       
			<div class="col-md-6">
				<article class="post type-post">
					<div class="post-thumbnail">
						<div id="postImgSlider" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#postImgSlider" data-slide-to="0" class="active"></li>
							</ol>
							<div class="carousel-inner"> 
								<div class="item active">
								@if($bartermedia->file)
                                   <div class="item"> <img alt="{{ $bartermedia->title }}" src="{{ $bartermedia->file }}" /></div>
                                 @endif
								</div>
							</div>
							<a class="slide-nav left" href="#postImgSlider" data-slide="prev"><i class="fa fa-long-arrow-left"></i></a>
							<a class="slide-nav right" href="#postImgSlider" data-slide="next"><i class="fa fa-long-arrow-right"></i></a>
						</div>
					</div>
				</article>
			</div>

			<aside class="widget widget_tag_cloud">
                <div class="stripe-full">
                    <h4 class="widget-title stripe-over stripe-blue"><span>Kelime</span> Havuzu</h4>
                </div>

				<div class="tagcloud">								
                    @if($seobartertags->count())
                        @foreach($seobarters as $key => $seobarter)
                        @foreach($seobarter->tags as $tag) <a href="#">#{{$tag->name}}</a>@endforeach
                        @endforeach
                    @endif
                </div><!--  /.tagcloud -->

            </aside>

			
			@endforeach
		</div>
	</div>
</section>
@endsection