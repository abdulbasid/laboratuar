@extends('layouts.outside')

@section('content')
<section id="site-page-header" class="site-page-header blog-header polygon-bg">
		<div class="overlay-effects box-pattern index-1"></div>
		<div class="container">
			<div class="page-header-content display-table-middle">
				<div class="inside-content vertical-middle">
					<h1 class="page-title text-center highlighted" style="text-shadow: 1px 1px  #000;">HAKKIMIZDA</h1>
				</div>
			</div><!--  /.page-header-content -->
			<div class="site-breadcrumb pull-right">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ route('welcome') }}">Anasayfa</a></li>
					
					<li class="breadcrumb-item active">Hakkımızda</li>
				</ol>
			</div>
        </div></section>

<section id="about-us" class="about-page section-padding" style="background:#fff;">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="about-description">
                    <h3 class="about-title bm30"><span>LABORATUAR</span> HAKKINDA </h3>
                    <p style="font-size: 16px !important;">
                        <span class="dropcap"><img src="{{ asset('web/images/ikonolcak.png')}}" alt="L"></span>
                        Ürün ya da hizmetlerinizden aldığınız gücü; medya
                        giderlerinizde modelleyebilmeniz için formüller üretiyoruz.
                        Hedef kitlenizi belirleyip, doğru mecralarda kısa sürede geri dönüş sağlıyoruz.
                        <b>Biz 17 yıldır formüllüyoruz...</b> Gelin siz de yeni nesil medya planlama ile vaktinizi ve nakdinizi koruyun.
                    </p>
                </div>
            </div>
            <div class="col-md-12 p0" style="margin-top: 50px">
                @foreach($aboutuses as $aboutus)
                    <div class="col-sm-6 p0">
                        <div class="col-xs-12 about-box">
                            <div class="col-sm-6 p0">
                            @if($aboutus->file)
                                <div class="item"> <img alt="{{ $aboutus->title }}" src="{{ $aboutus->file }}" /></div>
                            @endif
                               
                            </div>
                            <div class="col-sm-6">
                                <h2>{{ $aboutus->title }}</h2>
                                <p>{{ $aboutus->desciription }}
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach   
            </div>
            <aside class="widget widget_tag_cloud">
                <div class="stripe-full">
                    <h4 class="widget-title stripe-over stripe-blue"><span>Kelime</span> Havuzu</h4>
                </div>

				<div class="tagcloud">								
                    @if($seoabouttags->count())
                        @foreach($seoabouts as $key => $seoabout)
                        @foreach($seoabout->tags as $tag) <a href="#">#{{$tag->name}}</a>@endforeach
                        @endforeach
                    @endif
                </div><!--  /.tagcloud -->

            </aside>
        </div>
    </div>
</section>

@endsection
