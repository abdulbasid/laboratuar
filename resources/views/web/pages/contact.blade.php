@extends('layouts.outside')

@section('content')
<section id="site-page-header" class="site-page-header blog-header polygon-bg">
		<div class="overlay-effects box-pattern index-1"></div>
		<div class="container">
			<div class="page-header-content display-table-middle">
				<div class="inside-content vertical-middle">
					<h1 class="page-title text-center highlighted" style="text-shadow: 1px 1px  #000;">İLETİŞİM</h1>
				</div>
			</div><!--  /.page-header-content -->
			<div class="site-breadcrumb pull-right">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="{{ route('welcome') }}">Anasayfa</a></li>
					
					<li class="breadcrumb-item active">Bize Ulaşın</li>
				</ol>
			</div>
        </div></section>

<section style="background:#FFF" class="contact section-padding">
    <div class="container">
        <div class="title-container text-center">
            <h2 class="striped-title">
                BİZE <span>ULAŞIN</span>
                <span class="stripe-t-left"></span>
            
                <span class="stripe-t-right"></span>
            </h2>
            <p class="section-description">
                PROFESYONEL İŞLERE GİDEN YOL, PROFESYONEL BİR EKİPLE ÇALIŞMAKTAN GEÇER.
            </p>
        </div>

        <div class="row">
            <style>.item-container {height:290px;}</style>
            <div class="col-md-4 col-sm-6 text-center">
                <div class="item-container v2">
                    <div class="icon mauto">
                        <i class="fa fa-phone"></i>
                    </div>
                    <h5 class="feature-title stripe-bfixw">TELEFONLARIMIZ</h5>
                    <p>
                    @foreach($contacts as $contact)
                        Telefon	: {{ $contact->gsm }}<br>
                        Telefon : {{ $contact->gsm2 }}
                    @endforeach   
                    </p>
                </div>
            </div>


            <div class="col-md-4 col-sm-6 text-center">
                <div class="item-container v2">
                    <div class="icon mauto">
                        <i class="fa fa-home"></i>
                    </div>
                    <h5 class="feature-title stripe-bfixw">ADRES</h5>
                    <p>
                    @foreach($contacts as $contact)
                        {{ $contact->address }}
                    @endforeach  
                    </p>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 text-center">
                <div class="item-container v2">
                    <div class="icon mauto">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <h5 class="feature-title stripe-bfixw">E-POSTA & WEBSİTESİ</h5>
                    <p>
                    @foreach($contacts as $contact)
                        <a href="mailto:{{ $contact->email }}">{{ $contact->email }} </a><br>
                    @endforeach  
                    </p>
                </div> 
            </div>
        </div>
    </div>
</section>

<div id="google-map" class="contact v2">
    <div class="map-container">
        <div class="google-map-container">
        @foreach($contacts as $contact)
        <iframe src="{{ $contact->map }}" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
        @endforeach
        </div>
    </div>
</div>
@endsection