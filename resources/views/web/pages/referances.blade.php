@extends('layouts.outside')

@section('content')
<div class="page-banner" style="padding:100px 0; background-image: url({{ asset('web/images/referanslarimiz-wallpaper.jpg') }}); text-shadow: 1px 1px 1px white;">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <h1>REFERANSLAR</h1>
                <h2>Medya Reklamlarınızı Barter Formülüyle Geliştiriyoruz.</h2>
            </div>
            
        </div>
    </div>
</div>

<div id="content">
			<div class="container">
				<div class="row portfolio-page">
                    <div id="portfolio" class="portfolio-4" style="padding-top:50px;">
                    @foreach($referances as $referance)
                        <div class="portfolio-item web drawing col-md-3">
                        	<div class="portfolio-border">
                                <div class="portfolio-thumb">
                                  <a href="{{ route('project', $referance->slug) }}">
                                    @if($referance->file)
                                        <img alt="{{ $referance->name }}" src="{{ $referance->file }}" />
                                    @endif
                                </div>
                                <div class="portfolio-details">
                                        <h4 style="text-align:center;">{{ $referance->name }}</h4>
                                        <span>Çalışma Modelimizi görmek için tıklayınız</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
				</div>
			</div>
		</div>
<br/>
<br/>
<br/>
@endsection
