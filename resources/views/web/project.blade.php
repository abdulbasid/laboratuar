@extends('layouts.outside')

@section('content')


<section id="site-page-header" class="site-page-header blog-header polygon-bg">
    <div class="overlay-effects box-pattern index-1"></div>
    <div class="container">
        <div class="page-header-content display-table-middle">
            <div class="inside-content vertical-middle">
                <h1 class="page-title text-center highlighted" style="text-shadow: 1px 1px  #000;">{{ $project->name }} </h1>
            </div>
        </div>
    </div>
</section>






<section style="background:#FFF" id="about-us" class="about-page section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="about-description">
                    <h3 class="about-title bm30">{{ $project->name }} </h3>
                    <p> 
                    <span class="dropcap">
                        <img src="{{ asset('web/images/ikonolcak.png') }}" alt="M">
                    </span>
                    {{ $project->desciription }}
                    </p>
                </div>
            </div>

            <div class="col-md-6">
                <article class="post type-post">
                    <div class="post-thumbnail">
                        <div id="postImgSlider" class="carousel slide" data-ride="carousel">
                        
                            <div class="carousel-inner"> 
                                <div class="item active">
                                @if($project->file)   
                                <img alt="{{ $project->name }}" src="{{ $project->file }}">
                                @endif
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </article>
            </div>

            <aside class="widget widget_tag_cloud">
                <div class="stripe-full">
                    <h4 class="widget-title stripe-over stripe-blue"><span>Kelime</span> Havuzu</h4>
                </div>

                <div class="tagcloud">								
                    @if($seobartertags->count())
                        @foreach($seobarters as $key => $seoproject)
                        @foreach($seoproject->tags as $tag) <a href="#">#{{$tag->name}}</a>@endforeach
                        @endforeach
                    @endif
                </div><!--  /.tagcloud -->

            </aside>
        </div>
    </div>

</section>


        
@endsection