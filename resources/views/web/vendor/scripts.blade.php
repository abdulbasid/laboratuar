<script src="{{ asset('web/js/jquery-1.11.1.min.js') }}"></script>  
<script src="{{ asset('web/js/plugins.js') }}"></script>
<script src="{{ asset('web/js/isotope.pkgd.min.js') }}"></script> 
<script src="{{ asset('web/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('web/owl-carousel/owl.carousel.min.js') }}"></script> 
<script src="{{ asset('web/js/jquery.circliful.js') }}"></script>
<script src="{{ asset('web/js/jquery.fitvids.js') }}"></script>
<script src="{{ asset('web/js/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('web/js/jquery.jscrollpane.min.js') }}"></script>
<script src="{{ asset('web/js/superfish.min.js') }}"></script>
<script src="{{ asset('web/js/jquery.vgrid.min.js') }}"></script> 
<script src="{{ asset('web/js/jquery.parallax.js') }}"></script>
<script src="{{ asset('web/js/jquery.fs.boxer.min.js') }}"></script>
<script src="{{ asset('web/js/wow.min.js') }}"></script>
<script src="{{ asset('web/masterslider/jquery.easing.min.js') }}"></script>
     
  
    <script src="{{ asset('web/masterslider/masterslider.min.js') }}"></script>

    <script type="text/javascript">     
    (function ($) {
        "use strict";
        $(function () {
            var focuzSlider = new MasterSlider();
            
            focuzSlider.control('arrows');
            focuzSlider.control('timebar' , {insertTo:'#focuz-main-slider'});
          
            focuzSlider.setup("focuz-main-slider", {
                width           : 1140,
                height          : 600,
                space           : 0,
                start           : 1,
                layout          :'fullwidth',
                loop            : true,
                preload         : 0,
                autoplay        : true,
                centerControls  : false,
                view            : "mask"
            });
             
        });

        var clientReview = new MasterSlider();
        clientReview.setup('client-review-slider' , {
            loop:true,
            width:100,
            height:100,
            speed:50,
            view:'focus',
            preload:'all',
            space:10,
            wheel:true
        });
        clientReview.control('arrows',{insertTo:'#client-review-nav', autohide:false});
        clientReview.control('slideinfo',{insertTo:'#client-review-info'});
    })(jQuery);

    </script>
    <script src="{{ asset('web/js/custom.js') }}"></script> 
    
  
    
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-31048407-2', 'auto');
  ga('send', 'pageview');

</script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-92174940-5"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-92174940-5');
</script>
