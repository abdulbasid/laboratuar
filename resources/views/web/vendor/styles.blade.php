<script>
  $(document).ready(function(){
      $('#messagewindow').animate({
          scrollTop: $('#messagewindow')[0].scrollHeight}, 2000);
  });
</script>


<link rel="shortcut icon" href="{{ asset('web/images/favicon.ico') }}">

<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="{{ asset('web/js/modernizr.js') }}"></script>


<link href="{{ asset('web/css/preloader.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('web/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('web/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" /> 
<link href="{{ asset('web/css/linecons-font-style.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('web/css/jquery.fs.boxer.min.css') }}" rel="stylesheet" type="text/css" />  
<link href="{{ asset('web/owl-carousel/owl.carousel.css') }}" rel="stylesheet" type="text/css" > 
<link href="{{ asset('web/css/jquery.circliful.css') }}" rel="stylesheet" type="text/css" >
<link href="{{ asset('web/css/magnific-popup.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('web/css/jquery.jscrollpane.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('web/css/animate.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('web/masterslider/style/masterslider.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('web/masterslider/skins/default/style.css') }}" rel='stylesheet' type='text/css'>
<link href="{{ asset('web/css/slider.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('web/css/main.css') }}" rel="stylesheet" type="text/css"/> 
<link href="{{ asset('web/css/responsive.css') }}"rel="stylesheet" type="text/css"/>
<link href="{{ asset('web/lights/christmaslights.css') }}" rel="stylesheet" media="screen"  />

<style type="text/css">
  body {
    overflow-x:hidden;
  }
  .bora {
    color: #F00;
  }
  .yenilanyeni {
    color: #00a3c9;
  }
</style>
<script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');  ga('create', 'UA-87157727-3', 'auto'); ga('send', 'pageview');  </script>