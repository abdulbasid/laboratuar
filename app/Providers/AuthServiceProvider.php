<?php

namespace App\Providers;

use App\Post;
use App\Project;
use App\Slider;
use App\Aboutus;
use App\Contact;
use App\Referance;
use App\Service;
use App\HomePage;
use App\SeoAbout;
use App\SeoReferance;
use App\SeoContact;
use App\SeoBarter;
use App\SeoProject;
use App\ContactMessage;
use App\Setting;
use App\Policies\PostPolicy;
use App\Policies\ProjectPolicy;
use App\Policies\SliderPolicy;
use App\Policies\AboutusPolicy;
use App\Policies\ContactPolicy;
use App\Policies\ReferancePolicy;
use App\Policies\ServicePolicy;
use App\Policies\HomePagePolicy;
use App\Policies\SeoAboutPolicy;
use App\Policies\SeoReferancePolicy;
use App\Policies\SeoContactPolicy;
use App\Policies\ContactMessagePolicy;
use App\Policies\SettingPolicy;
use App\Policies\SeoBarterPolicy;
use App\Policies\SeoProjectPolicy;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Post::class => PostPolicy::class,
        Project::class => ProjectPolicy::class,
        Slider::class => SliderPolicy::class,
        Aboutus::class => AboutusPolicy::class,
        Contact::class => ContactPolicy::class,
        Referance::class => ReferancePolicy::class,
        Service::class => ServiceePolicy::class,
        HomePage::class => HomePagePolicy::class,
        SeoAbout::class => SeoAboutPolicy::class,
        SeoReferance::class => SeoReferancePolicy::class,
        SeoContact::class => SeoContactPolicy::class,
        ContactMessage::class => ContactMessagePolicy::class,
        Setting::class => SettingPolicy::class,
        SeoBarter::class => SeoBarterPolicy::class,
        SeoProject::class => SeoProjectPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
