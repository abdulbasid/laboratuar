<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'address' ,'email','gsm','gsm2', 'map','youtube','linkedin','instagram','facebook','twitter'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
