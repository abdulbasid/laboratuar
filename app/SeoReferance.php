<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeoReferance extends Model
{

    use \Conner\Tagging\Taggable;
    
    protected $table = 'seo_referances';
    protected $fillable = [
        'title', 'desciription'
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
