<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeoBarter extends Model
{

    use \Conner\Tagging\Taggable;
    
    protected $table = 'seo_barters';
    protected $fillable = [
        'title', 'desciription'
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
