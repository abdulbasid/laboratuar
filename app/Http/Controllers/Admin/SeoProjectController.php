<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\SeoProjectStoreRequest;
use App\Http\Requests\SeoProjectUpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\SeoProject;
use App\Tag;

class SeoProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seoprojects = SeoProject::orderBy('id', 'DESC')
            ->paginate();
            $seoprojectcounts = SeoProject::All('id')->count();
            

        return view('admin.seoprojects.index', compact('seoprojects','seoprojectcounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.seoprojects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SeoProjectStoreRequest $request)
    {
        $tags = explode(",", $request->tags);


    	$seoproject = SeoProject::create($request->all());
        $seoproject->tag($tags);


        return redirect()->route('seoprojects.index', $seoproject->id)->with('info', 'Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $seoproject = SeoProject::find($id);

        return view('admin.seoprojects.show', compact('seoproject'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seoproject       = SeoProject::find($id);

        return view('admin.seoprojects.edit', compact('seoproject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SeoProjectUpdateRequest $request, $id)
    {
        $seoproject = SeoProject::find($id);
        $seoproject->fill($request->all())->save();

        $tags = explode(",", $request->tags);

        $seoproject->tag($tags);

        return redirect()->route('seoprojects.index', $seoproject->id)->with('info', 'Başarıyla Güncellendi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $seoproject = SeoProject::find($id)->delete();

        return back()->with('info', 'Başarıyla Silindi');
    }
}
