<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ReferanceStoreRequest;
use App\Http\Requests\ReferanceUpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\Referance;

class ReferanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $referances = Referance::orderBy('id', 'DESC')
            ->paginate();

        return view('admin.referanslar.index', compact('referances'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.referanslar.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReferanceStoreRequest $request)
    {
        $tags = explode(",", $request->tags);


    	$referance = Referance::create($request->all());
        $referance->tag($tags);

        //IMAGE 
        if($request->file('image')){
            $path = Storage::disk('public')->put('image',  $request->file('image'));
            $referance->fill(['file' => asset($path)])->save();
        }

      

        return redirect()->route('referanslar.index', $referance->id)->with('info', 'Referans Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $referance = Referance::find($id);

        return view('admin.referanslar.show', compact('referance'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $referance       = Referance::find($id);

        return view('admin.referanslar.edit', compact('referance'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ReferanceUpdateRequest $request, $id)
    {
        $referance = Referance::find($id);
        $referance->fill($request->all())->save();

        $tags = explode(",", $request->tags);

        $referance->tag($tags);

        //IMAGE 
        if($request->file('image')){
            $path = Storage::disk('public')->put('image',  $request->file('image'));
            $referance->fill(['file' => asset($path)])->save();
        }

        return redirect()->route('referanslar.index', $referance->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $referance =Referance::where('id',$id)->first();

        if ($referance != null) {
            $referance->delete();
            return redirect()->route('referanslar.index')->with(['info'=> 'Başarıyla Silindi']);
        }

        return redirect()->route('referanslar.index')->with(['info'=> 'Hatalı ID']);
    }
}
