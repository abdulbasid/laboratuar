<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\SeoBarterStoreRequest;
use App\Http\Requests\SeoBarterUpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\SeoBarter;
use App\Tag;

class SeoBarterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seobarters = SeoBarter::orderBy('id', 'DESC')
            ->paginate();
            $seobartercounts = SeoBarter::All('id')->count();
            

        return view('admin.seobarters.index', compact('seobarters','seobartercounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.seobarters.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SeoBarterStoreRequest $request)
    {
        $tags = explode(",", $request->tags);


    	$seobarter = SeoBarter::create($request->all());
        $seobarter->tag($tags);


        return redirect()->route('seobarters.index', $seobarter->id)->with('info', 'Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $seobarter = SeoBarter::find($id);

        return view('admin.seobarters.show', compact('seobarter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seobarter       = SeoBarter::find($id);

        return view('admin.seobarters.edit', compact('seobarter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SeoBarterUpdateRequest $request, $id)
    {
        $seobarter = SeoBarter::find($id);
        $seobarter->fill($request->all())->save();

        $tags = explode(",", $request->tags);

        $seobarter->tag($tags);

        return redirect()->route('seobarters.index', $seobarter->id)->with('info', 'Başarıyla Güncellendi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $seobarter = SeoBarter::find($id)->delete();

        return back()->with('info', 'Başarıyla Silindi');
    }
}
