<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\MedyaBarterStoreRequest;
use App\Http\Requests\MedyaBarterUpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\MedyaBarter;

class MedyaBarterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bartermedias = MedyaBarter::orderBy('id', 'DESC')
            ->paginate();

        $barterrowcounts = MedyaBarter::All('id')->count();
        return view('admin.bartermedias.index', compact('bartermedias','barterrowcounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.bartermedias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MedyaBarterStoreRequest $request)
    {
        $bartermedia = MedyaBarter::create($request->all());
      

        //IMAGE 
        if($request->file('image')){
            $path = Storage::disk('public')->put('image',  $request->file('image'));
            $bartermedia->fill(['file' => asset($path)])->save();
        }

      

        return redirect()->route('bartermedias.index', $bartermedia->id)->with('info', 'Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bartermedia = MedyaBarter::find($id);
       

        return view('admin.bartermedias.show', compact('bartermedia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $bartermedia       = MedyaBarter::find($id);
        

        return view('admin.bartermedias.edit', compact('bartermedia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MedyaBarterUpdateRequest $request, $id)
    {
        $bartermedia = MedyaBarter::find($id);
       

        $bartermedia->fill($request->all())->save();

        //IMAGE 
        if($request->file('image')){
            $path = Storage::disk('public')->put('image',  $request->file('image'));
            $bartermedia->fill(['file' => asset($path)])->save();
        }

        return redirect()->route('bartermedias.index', $bartermedia->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bartermedia =MedyaBarter::where('id',$id)->first();


        if ($bartermedia != null) {
            $bartermedia->delete();
            return redirect()->route('bartermedias.index')->with(['info'=> 'Başarıyla Silindi']);
        }

        return redirect()->route('bartermedias.index')->with(['info'=> 'Hatalı ID']);
        }
    
}
