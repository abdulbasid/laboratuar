<?php

namespace App\Http\Controllers\Web;

use App\Aboutus;
use App\Service;
use App\SeoAbout;
use App\Setting;
use App\AllTags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutUsController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $aboutuses = Aboutus::All();
        $services = Service::All();
        $settings = Setting::All();
        $seoabouts = SeoAbout::All();
        $seoabouttags = AllTags::All()->where('taggable_type','App\\SeoAbout');

    	return view('web.pages.about-us', compact('aboutuses','services','seoabouts','seoabouttags','settings'));
    }
}
