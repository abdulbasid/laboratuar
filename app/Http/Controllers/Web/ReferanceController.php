<?php

namespace App\Http\Controllers\Web;

use App\Project;
use App\Contact;
use App\Service;
use App\SeoReferance;
use App\AllTags;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReferanceController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function referance(){
        $referances = Project::All();
        $contacts = Contact::All();
        $services = Service::All();
        $settings = Setting::All();
        $seoreferances = SeoReferance::All();
        $seoreferancestags = AllTags::All()->where('taggable_type','App\\SeoReferance');

    	return view('web.pages.referances', compact('referances','contacts','services','seoreferances','seoreferancestags','settings'));
    }
}
