<?php

namespace App\Http\Controllers\Web;

use App\MedyaBarter;
use App\Service;
use App\Setting;
use App\SeoBarter;
use App\AllTags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BarterTakasController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function index(){

        $bartermedias = MedyaBarter::All();
        $services = Service::All();
        $settings = Setting::All();
        $seobarters = SeoBarter::All();
        $seobartertags = AllTags::All()->where('taggable_type','App\\SeoBarter');

    	return view('web.pages.barter-takas', compact('bartermedias','services','settings','seobarters','seobartertags'));
    }
}
