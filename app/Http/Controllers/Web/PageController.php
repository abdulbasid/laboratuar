<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Tag;
use App\Post;
use App\Project;
use App\Slider;
use App\Contact;
use App\Referance;
use App\Service;
use App\HomePage;
use App\SeoAbout;
use App\SeoReferance;
use App\SeoContact;
use App\AllTags;
use App\Setting;
use App\SeoProject;
use App\SeoService;


class PageController extends Controller
{
    
    public function blog(){
        $posts = Post::orderBy('id', 'DESC')->where('status', 'PUBLISHED')->paginate(3);
        $projects = Project::All();
        $services = Project::All();
        $sliders = Slider::All();
        $contacts = Contact::All();
        $referances = Referance::All();
        $services = Service::All();
        $seoabouts = SeoAbout::All();
        $seoreferances = SeoReferance::All();
        $seocontacts = SeoContact::All();
        $settings = Setting::All();
        $homepages = HomePage::All();
        $hometags = AllTags::All()->where('taggable_type','App\\HomePage');

        
    	return view('web.posts', compact('posts','projects','sliders','contacts','referances','services','homepages','seoabouts','seoreferances','seocontacts','hometags','settings'));
    }

    public function category($slug){
        $category = Category::where('slug', $slug)->pluck('id')->first();

        $posts = Post::where('category_id', $category)
            ->orderBy('id', 'DESC')->where('status', 'PUBLISHED')->paginate(3);

        return view('web.posts', compact('posts'));
    }

    public function tag($slug){ 
        $posts = Post::whereHas('tags', function($query) use ($slug) {
            $query->where('slug', $slug);
        })
        ->orderBy('id', 'DESC')->where('status', 'PUBLISHED')->paginate(3);

        return view('web.posts', compact('posts'));
    }

    public function post($slug){
    	$post = Post::where('slug', $slug)->first();

    	return view('web.post', compact('post'));
    }
    

    public function referance($slug){
    	$referance = Referance::where('slug', $slug)->first();
        $referances = Referance::All();
        $services = Service::All();
        $seoreferances = SeoReferance::All();
        $seoreferancestags = AllTags::All()->where('taggable_type','App\\SeoReferance');
        $settings = Setting::All();

    	return view('web.referance', compact('referance','referances','services','seoreferances','seoreferancestags','settings'));
    }

    public function service($slug){
    	$service = Service::where('slug', $slug)->first();
        $services = Service::All();
        $settings = Setting::All();
        $seoservices = Service::All();
        $seoservicetags = AllTags::All()->where('taggable_type','App\\SeoService');

    	return view('web.service', compact('service','services','settings','seoservices','seoservicetags'));
    }

    public function project($slug){
    	$project = Project::where('slug', $slug)->first();
        $projects = Project::All();
        $contacts = Contact::All();
        $services = Service::All();
        $seoprojects = SeoProject::All();
        $seoprojectstags = AllTags::All()->where('taggable_type','App\\SeoProject');
        $settings = Setting::All();
    	return view('web.project', compact('project','projects','contacts','services','seoprojects','seoprojectstags','settings'));
    }

}
