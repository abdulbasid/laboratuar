<?php

namespace App\Http\Controllers\Web;

use App\Contact;
use App\Service;

use App\SeoContact;
use App\AllTags;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        
        $contacts = Contact::All();
        $services = Service::All();
        $settings = Setting::All();
        $seocontacts = SeoContact::All();
        $contacttags = AllTags::All()->where('taggable_type','App\\SeoContact');

    	return view('web.pages.contact', compact('contacts','services','seocontacts','contacttags','settings'));
    }
}
