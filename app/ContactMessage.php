<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactMessage extends Model
{
    public $table = 'contact_mes';
    protected $fillable = [
        'name' ,'email','message','viewed'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
