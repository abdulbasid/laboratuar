<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeoProject extends Model
{

    use \Conner\Tagging\Taggable;
    
    protected $table = 'seo_projects';
    protected $fillable = [
        'title', 'desciription'
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
