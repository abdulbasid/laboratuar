<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomePage extends Model
{

    use \Conner\Tagging\Taggable;
    
    protected $table = 'home';
    protected $fillable = [
        'title', 'desciription'
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
