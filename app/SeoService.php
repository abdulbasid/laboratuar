<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeoService extends Model
{

    use \Conner\Tagging\Taggable;
    
    protected $table = 'seo_services';
    protected $fillable = [
        'title', 'desciription'
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
