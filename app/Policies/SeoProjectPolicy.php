<?php

namespace App\Policies;

use App\SeoProject;
use Illuminate\Auth\Access\HandlesAuthorization;

class SeoProjectPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(SeoProject $seoproject)
    {
        return $seoproject->id;
    }
}
