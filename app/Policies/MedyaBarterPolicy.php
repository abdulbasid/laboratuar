<?php

namespace App\Policies;

use App\MedyaBarter;
use Illuminate\Auth\Access\HandlesAuthorization;

class MedyaBarterPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(User $user, MedyaBarter $medyabarter)
    {
        return $medyabarter->id;
    }
}
