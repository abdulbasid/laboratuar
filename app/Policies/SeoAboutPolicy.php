<?php

namespace App\Policies;

use App\HomePage;
use Illuminate\Auth\Access\HandlesAuthorization;

class SeoAboutPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(SeoAbout $seoabout)
    {
        return $seoabout->id;
    }
}
