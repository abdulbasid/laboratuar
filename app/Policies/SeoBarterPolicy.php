<?php

namespace App\Policies;

use App\SeoBarter;
use Illuminate\Auth\Access\HandlesAuthorization;

class SeoBarterPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(SeoBarter $seobarter)
    {
        return $seobarter->id;
    }
}
