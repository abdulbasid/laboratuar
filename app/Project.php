<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use \Conner\Tagging\Taggable;

    protected $fillable = [
        'desciription','name','slug','file'
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
