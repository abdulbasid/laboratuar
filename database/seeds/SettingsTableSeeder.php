<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Laboratuar Medya Ajansı - Sinema Reklamı, Radyo Reklamı, Televizyon Reklamında doğru seçim.',
                'favicon' => 'http://localhost/laboratuar/public/image/KzmiiCujMaDg2r9HpHA5buhyBLduxxKYWhxAD7qi.ico',
                'logoalt' => 'Laboratuar Medya Ajansı',
                'logo' => 'http://localhost/laboratuar/public/image/oGIceSJg8PDhvMfCvcGXWYa72KWRegUA6gkpqKbu.png',
                'created_at' => '2019-12-23 08:14:57',
                'updated_at' => '2019-12-23 08:14:57',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}