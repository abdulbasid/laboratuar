<?php

use Illuminate\Database\Seeder;

class AboutusesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('aboutuses')->delete();
        
        \DB::table('aboutuses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'TELEVİZYON',
                'desciription' => 'Reklam bütçelerinin aslan payını alan televizyon, günde ortalama 330 dakika izlenme oranına sahip. Televizyon reklamı planlama stratejisinde ki formüllerimiz ile tanışın. Kitle analizinde doğru frekansı yakalayın.',
                'file' => 'http://localhost/laboratuar/public/image/Hx06bMc2lWFofadMtE1zRd94AAbl2UacAAGg0ElG.jpeg',
                'created_at' => '2019-11-20 00:27:02',
                'updated_at' => '2019-11-21 20:02:50',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'SİNEMA',
                'desciription' => '45 Şehirde, 1288 salonda ayda ortalama 8,5 milyon, yılda ortalama 65 milyon kişiye ulaşıyoruz. Üstelik ışıklar kapanıp da filmi merakla beklediğimiz özel reklam kuşağındayız',
                'file' => 'http://localhost/laboratuar/public/image/5e3ZjNg1dasfiiaX4zolOz7wepC511rK3BzSgLYZ.jpeg',
                'created_at' => '2019-11-21 19:56:30',
                'updated_at' => '2019-11-21 19:56:30',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'RADYO',
                'desciription' => 'Kendisine ayrıca zaman ayırmanızı beklemeyen, günün her anı sizi takip edebilen bir mecranın; doğru planlandığında neler yapabileceğini tecrübelerimizden biliyoruz.',
                'file' => 'http://localhost/laboratuar/public/image/yHTN4HqSfBTznKj4ZfJUntIsWlUJ7wHbdZNW9Niu.jpeg',
                'created_at' => '2019-11-21 19:56:44',
                'updated_at' => '2019-11-21 19:56:52',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'FİKİR & PRODÜKSİYON',
                'desciription' => 'Fikir ve prodüksyon arasında ki bağ iyi uygulandığında kısa sürede etkili sonuç verir. Bu konuda önemli olan doğru ve fark yaratacak fikri uygun maliyetli ve yüksek kaliteli prodüksiyonla buluşturmanın formülüdür. Bu da La’boratuar da fazlasıyla var.',
                'file' => 'http://localhost/laboratuar/public/image/cfivfmEPqKZnhuzWUZdWC5EtxWnFfP0pPuNa9sca.jpeg',
                'created_at' => '2019-11-21 19:57:05',
                'updated_at' => '2019-11-21 19:57:05',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'AÇIK HAVA',
                'desciription' => 'Caddelerde, sokaklarda, duvarlarda, bazen geçen bir otobüsün hemen ardında, günün kalabalığına eşlik ediyor, geceyi markanızla aydınlatıyoruz.',
                'file' => 'http://localhost/laboratuar/public/image/IXScKBxfq7izlJASAfR2nB2j32X7ACwNx4SwXzEk.jpeg',
                'created_at' => '2019-11-21 19:57:21',
                'updated_at' => '2019-11-21 19:57:21',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'DİJİTAL',
                'desciription' => 'Markanızı dijital platformlarda en doğru şekilde konumlandırıyoruz. Kreatif marka stratejinizi oluşturarak, gerçek zamanlı dijital pazarlama yöntemiyle doğru hedef kitle ile markanızı buluşturuyoruz.',
                'file' => 'http://localhost/laboratuar/public/image/JszdN3uJbxzPg9v7yuNwe0h5TSjnjhjFbxDq81Cg.jpeg',
                'created_at' => '2019-11-21 19:57:42',
                'updated_at' => '2019-11-21 19:57:42',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}