<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('contacts')->delete();
        
        \DB::table('contacts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'address' => 'Mehmet Nezih Özmen Mah.Fatih Cad. Karadal Sk.No:7 Kat:4 Merter 34600 İstanbul Türkiye',
                'email' => 'info@bartermedya.com.tr',
            'gsm' => '+90 ( 0212 ) 504 06 81',
            'gsm2' => '+90 ( 0212 ) 504 06 82',
                'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3010.4285611123432!2d28.88244231568035!3d41.015879026905466!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cabb1f3709a01b%3A0xec2c19188cf5e920!2sLaboratuar%20Reklam%20Ajans%C4%B1!5e0!3m2!1str!2str!4v1571393392704!5m2!1str!2str',
                'file' => NULL,
                'created_at' => '2019-11-20 01:54:44',
                'updated_at' => '2019-11-20 02:05:10',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}