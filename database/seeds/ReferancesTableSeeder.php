<?php

use Illuminate\Database\Seeder;

class ReferancesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('referances')->delete();
        
        \DB::table('referances')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'KİĞILI',
                'slug' => 'kigili',
                'desciription' => 'KİĞILI',
                'file' => 'http://localhost/laboratuar/public/image/NHwB4iHgTs4pEy8ETdjJmVyqQnhE4X42ehQLbPVp.png',
                'created_at' => '2019-11-21 17:45:14',
                'updated_at' => '2019-11-21 17:45:15',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'SPX',
                'slug' => 'spx',
                'desciription' => 'SPX',
                'file' => 'http://localhost/laboratuar/public/image/p2P4opNUHmgseSyMBUfWbk93T9MVXcXIeWh1B6jP.png',
                'created_at' => '2019-11-21 17:47:46',
                'updated_at' => '2019-11-21 17:47:46',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'BG STORE',
                'slug' => 'bg-store',
                'desciription' => 'BG STORE',
                'file' => 'http://localhost/laboratuar/public/image/4HprFWgXzkobLfZnEetTbSj2VRotGlrflUmGSbTt.png',
                'created_at' => '2019-11-21 17:48:05',
                'updated_at' => '2019-11-21 17:48:05',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'DAMAT',
                'slug' => 'damat',
                'desciription' => 'BG STORE',
                'file' => 'http://localhost/laboratuar/public/image/0A32uD7KxLkAVVS28NXfWL0fKEicHvHDI63JMoh5.png',
                'created_at' => '2019-11-21 17:48:19',
                'updated_at' => '2019-11-21 17:48:19',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'DAP YAPI',
                'slug' => 'dap-yapi',
                'desciription' => 'DAP YAPI',
                'file' => 'http://localhost/laboratuar/public/image/Psf7vwJv1yglkTuayhS4sYvVNpk5WKnxkMCaTozr.png',
                'created_at' => '2019-11-21 17:48:38',
                'updated_at' => '2019-11-21 17:48:38',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'AVVA',
                'slug' => 'avva',
                'desciription' => 'AVVA',
                'file' => 'http://localhost/laboratuar/public/image/R3U3pcDsUhwSCO2nr95TNmHlDs47tmBmMsOIC1uk.png',
                'created_at' => '2019-11-21 17:48:52',
                'updated_at' => '2019-11-21 17:48:52',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'LUİFAN',
                'slug' => 'luifan',
                'desciription' => 'LUİFAN',
                'file' => 'http://localhost/laboratuar/public/image/viAjAzhojmVh9wUr3rtEhppYwUyaBz5La0q3GGYr.png',
                'created_at' => '2019-11-21 17:49:10',
                'updated_at' => '2019-11-21 17:49:10',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'TURA TURİZM',
                'slug' => 'tura-turizm',
                'desciription' => 'TURA TURİZM',
                'file' => 'http://localhost/laboratuar/public/image/1SL6NuNnnpIVqeMBUsP0enZDTtKWWQxW0sMjnsjk.png',
                'created_at' => '2019-11-21 17:49:23',
                'updated_at' => '2019-11-21 17:49:23',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'ÇİÇEK SEPETİ',
                'slug' => 'cicek-sepeti',
                'desciription' => 'ÇİÇEK SEPETİ',
                'file' => 'http://localhost/laboratuar/public/image/RDQ2R3YWCHCNV5eyawbeGFTd3f6toYOmjxlNYHST.png',
                'created_at' => '2019-11-21 17:49:45',
                'updated_at' => '2019-11-21 17:49:45',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'DESA',
                'slug' => 'desa',
                'desciription' => 'DESA',
                'file' => 'http://localhost/laboratuar/public/image/Ubd6BqbYp9ekMGuztRsTJ8HGoyZsg2z6GaMyWEOG.png',
                'created_at' => '2019-11-21 17:49:58',
                'updated_at' => '2019-11-21 17:49:58',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'MATRAŞ',
                'slug' => 'matras',
                'desciription' => 'MATRAŞ',
                'file' => 'http://localhost/laboratuar/public/image/RoqixBLpK2zeqbOARa3dw7mE8ZwPNcIl7TkqnBg9.png',
                'created_at' => '2019-11-21 17:50:24',
                'updated_at' => '2019-11-21 17:50:24',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'LAPIDEN',
                'slug' => 'lapiden',
                'desciription' => 'LAPIDEN',
                'file' => 'http://localhost/laboratuar/public/image/Gvf6GYmgnO05vOyt8i1MiOFlAPTs5OFfSENZY4uQ.png',
                'created_at' => '2019-11-21 17:50:41',
                'updated_at' => '2019-11-21 17:52:51',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'THE NORTH FACE',
                'slug' => 'the-north-face',
                'desciription' => 'THE NORTH FACE',
                'file' => 'http://localhost/laboratuar/public/image/C4MaBb56a34mt5P0JHi6XBMswa7ecFlrLCKDw5pV.png',
                'created_at' => '2019-11-21 17:53:08',
                'updated_at' => '2019-11-21 17:53:08',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'EĞİTİM BİLİMLERİ',
                'slug' => 'egitim-bilimleri',
                'desciription' => 'EĞİTİM BİLİMLERİ',
                'file' => 'http://localhost/laboratuar/public/image/MwyEA0eOlpgpKhyz8nYQKoogPBTZenDQ22rWi7u5.png',
                'created_at' => '2019-11-21 17:53:26',
                'updated_at' => '2019-11-21 17:53:26',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'ENGLISH TIME',
                'slug' => 'english-time',
                'desciription' => 'ENGLISH TIME',
                'file' => 'http://localhost/laboratuar/public/image/ihRW15LXm2Ebjfr59ZaTtEI467q6vHU1xiPk52Sr.png',
                'created_at' => '2019-11-21 17:53:37',
                'updated_at' => '2019-11-21 17:53:37',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'JACK WOLFSKIN',
                'slug' => 'jack-wolfskin',
                'desciription' => 'JACK WOLFSKIN',
                'file' => 'http://localhost/laboratuar/public/image/WIo5p3FRoBuu9F4btlgfvB12Au340U1fqB0UsfoA.png',
                'created_at' => '2019-11-21 17:53:54',
                'updated_at' => '2019-11-21 17:53:54',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'QUİK ILVER',
                'slug' => 'quik-ilver',
                'desciription' => 'QUİK ILVER',
                'file' => 'http://localhost/laboratuar/public/image/knmxgKbgHE7LsrOGfKMxxvBKsE6NgHnD022aAQfC.png',
                'created_at' => '2019-11-21 17:54:16',
                'updated_at' => '2019-11-21 17:54:16',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'TEPE HOME',
                'slug' => 'tepe-home',
                'desciription' => 'TEPE HOME',
                'file' => 'http://localhost/laboratuar/public/image/icB9qYrxuPx0hF1x22hWD52iMUkIHUBl6xHzioVr.png',
                'created_at' => '2019-11-21 17:54:29',
                'updated_at' => '2019-11-21 17:54:29',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'SALOMON',
                'slug' => 'salomon',
                'desciription' => 'SALOMON',
                'file' => 'http://localhost/laboratuar/public/image/okAQXEhZlHuZ1LVYmRgZru0fx9eFilBLnKbETgpm.png',
                'created_at' => '2019-11-21 17:54:42',
                'updated_at' => '2019-11-21 17:54:42',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'MERRELL',
                'slug' => 'merrell',
                'desciription' => 'MERRELL',
                'file' => 'http://localhost/laboratuar/public/image/60z3xuFgPNczgb3NMUuihAmxhQQG4ejpF3TXNcPw.png',
                'created_at' => '2019-11-21 17:54:55',
                'updated_at' => '2019-11-21 17:54:55',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'KLİT GLOBAL',
                'slug' => 'klit-global',
                'desciription' => 'KLİT GLOBAL',
                'file' => 'http://localhost/laboratuar/public/image/2wbMmU62CnGoGDyHA9BV4m33OeWJoQhwOW7d9zjr.png',
                'created_at' => '2019-11-21 17:55:51',
                'updated_at' => '2019-11-21 17:55:51',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'CRYSTAL HOTELS',
                'slug' => 'crystal-hotels',
                'desciription' => 'CRYSTAL HOTELS',
                'file' => 'http://localhost/laboratuar/public/image/DzmfQAbOuz28KjpBYJHfysUCH43EldgqrF00x25q.png',
                'created_at' => '2019-11-21 17:56:08',
                'updated_at' => '2019-11-21 17:56:08',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'AMARA WORLD HOTELS',
                'slug' => 'amara-world-hotels',
                'desciription' => 'AMARA WORLD HOTELS',
                'file' => 'http://localhost/laboratuar/public/image/1Xc2golBGlT0UeSMbjJDR9pHhX4iBsFpYQSdc1p8.png',
                'created_at' => '2019-11-21 17:56:26',
                'updated_at' => '2019-11-21 17:56:26',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'NIRVANA',
                'slug' => 'nirvana',
                'desciription' => 'NIRVANA',
                'file' => 'http://localhost/laboratuar/public/image/DPhcDE9QdKPchbr3S4ePJhwZZpOOM8nP1EEzv0El.png',
                'created_at' => '2019-11-21 17:56:43',
                'updated_at' => '2019-11-21 17:56:43',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'ÇAM GRUP',
                'slug' => 'cam-grup',
                'desciription' => 'ÇAM GRUP',
                'file' => 'http://localhost/laboratuar/public/image/gc8FZEWFTTTa7rxApjE7Bivbs2NUNo0XPxhptcOF.png',
                'created_at' => '2019-11-21 17:56:55',
                'updated_at' => '2019-11-21 17:56:55',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'HEDEF GRUP',
                'slug' => 'hedef-grup',
                'desciription' => 'HEDEF GRUP',
                'file' => 'http://localhost/laboratuar/public/image/MISnBVGlRb5lNbd5EciUYttjed0hrfzQGsuI7vJU.png',
                'created_at' => '2019-11-21 17:57:10',
                'updated_at' => '2019-11-21 17:57:10',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'HAKMAR EXPRESS',
                'slug' => 'hakmar-express',
                'desciription' => 'HAKMAR EXPRESS',
                'file' => 'http://localhost/laboratuar/public/image/wMuQbT0B1x4CA9kQOaTPjYRRKMkxpicmHVlb1qzU.png',
                'created_at' => '2019-11-21 17:57:26',
                'updated_at' => '2019-11-21 17:57:26',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'KAYA ÇİFTİĞİ',
                'slug' => 'kaya-ciftigi',
                'desciription' => 'KAYA ÇİFTİĞİ',
                'file' => 'http://localhost/laboratuar/public/image/sQ3Qcj7I4IMlmg7byX2GknYOIsemSYUySG4zN7Qd.png',
                'created_at' => '2019-11-21 17:57:42',
                'updated_at' => '2019-11-21 17:57:42',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'GASTRO ANTEP',
                'slug' => 'gastro-antep',
                'desciription' => 'GASTRO ANTEP',
                'file' => 'http://localhost/laboratuar/public/image/x6zTpgGUhEeCQdaUWN7GdQDmFTNzg7pQJ4EDLMzF.png',
                'created_at' => '2019-11-21 17:58:01',
                'updated_at' => '2019-11-21 17:58:01',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}