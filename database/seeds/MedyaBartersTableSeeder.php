<?php

use Illuminate\Database\Seeder;

class MedyaBartersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('medya_barters')->delete();
        
        \DB::table('medya_barters')->insert(array (
            0 => 
            array (
                'id' => 1,
            'title' => 'MEDYA BARTER (TAKAS)',
                'desciription' => 'Medya satın alma, planlama, prodüksiyon hizmetleri ve mecra kullanımların da takas formüllerimiz ile tanışın.

Nakdinizi koruyan, ürün veya hizmetlerinizle, medya reklam giderlerinizi karşılamanızın formülü bizde; üstelik 17 Yıldır.

Laboratuar olarak müşterilerimiz arasında yer alan tüm firmalarımızla bu formülü buluşturmanın haklı gururunu yaşıyoruz.',
                'file' => 'http://localhost/laboratuar/public/image/nowSn3vOHS5PaaB0Z8iNt2YsthXAjbg7Z29JF9PG.jpeg',
                'created_at' => '2019-11-21 23:20:04',
                'updated_at' => '2019-11-21 23:20:05',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}