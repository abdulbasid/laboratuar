<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('services')->delete();
        
        \DB::table('services')->insert(array (
            0 => 
            array (
                'id' => 1,
                'main_title' => 'KİTLE ANALİZİNDE DOĞRU FREKANS',
                'slug' => 'medya-planlama',
                'title' => 'MEDYA PLANLAMA',
                'desciription' => 'Laboratuar Mind Craft’ta reklam bütçenizi laboratuar analizi hassasiyetinde inceleyip eliyoruz.

Sinema, televizyon, açık hava, radyo ve dijital mecralarda ürün ve kitle analizi raporlarını karşılaştırıp doğru planlamayı etkinleştiriyoruz.

Etkin planlama ve doğru bütçe kullanımı ile medya planlamada 17 yılı geride bırakan ajansımız, ulusal reklam veren müşterilerine her geçen gün yenilerini ekleyip gücüne güç katıyor.',
                'file' => 'http://localhost/laboratuar/public/image/y1OmhVW1YDgAYk0TSSlKz0NXlAeAKpHIsFUZQbKu.jpeg',
                'created_at' => '2019-11-21 21:47:07',
                'updated_at' => '2019-11-21 21:47:08',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'main_title' => 'TELEVİZYON REKLAMLARI',
                'slug' => 'televizyon-reklamlari',
                'title' => 'TELEVİZYON REKLAMLARI',
                'desciription' => 'Televizyon reklamları tüm reklam tarihi boyunca en etkili reklamlar arasında olmuştur. İnternet çağının getirdiği dijitalleşme sürecinde televizyon kanallarının tahtı her geçen gün sarsılsa da, önümüzde ki birkaç yıl boyunca etkisinin zirvede olacağı tahmin edilmektedir. Bu süreçte dijitalleşen televizyon kanalları işin öncüleri arasında olacaktır.

Televizyon izleyicilerinin istediği içeriği istediği zaman izleme özgürlüğüne sahip oldukları platformlar hizmet vermeye başladıkça televizyon bir yansıtıcı olma yolunda hızlı ilerleyiş kaydetmiştir.

Televizyonun dijital dünyanın yansıtıcısı olma yolundaki hızlı ilerleyişi televizyon kanallarına ayrılan bütçesinin dijitale yönelmesindeki en etkili faktördür.',
                'file' => 'http://localhost/laboratuar/public/image/6H2fx9HLQ7L8JczL0Q1MI8RcLpzdk8rMziBcGwKP.jpeg',
                'created_at' => '2019-11-21 21:48:08',
                'updated_at' => '2019-11-21 21:48:08',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'main_title' => 'SİNEMA REKLAMLARI',
                'slug' => 'sinema-reklamlari',
                'title' => 'SİNEMA REKLAMLARI',
                'desciription' => 'Sinema izleyicisinin sayısı her geçen yıl güçlü artış göstermektedir. Türkiye de sinema salonlarına ciddi yatırımlar yapılmakta olup; dünya standartlarının üzerinde konforlu salonlar açılmakta ve yenilenmektedir. 2018 yılında sinema izleyicisi 70 Milyonun üzerindedir.

Yabancı yatırımcılardan CGV nin Türkiye de 850 Milyon Dolar yatırımı bulunmaktadır. Laboratuar olarak CGV Mars Media ile hacimli çalışmalar gerçekleştiriyoruz. Sinemaya yeni reklam veren firmaların yanı sıra, hali hazırda reklam veren firma bütçelerini, barter çözümlerimizle ile arttırıyoruz. Yılık anlaşmalarla sahibi olduğumuz özel reklam alanı, filme en yakın Platinium kuşak içerisinde yer almaktadır.

Sinema reklamı konum ve film bazlı optimize edildiğinden sadece ulusal reklam verenlerin değil, küçük ve orta ölçekli reklam verenlerin de işletmelerine yakın konumda reklam yaparak hızlı ve etkili dönüş almalarında etkili olmaktadır.',
                'file' => 'http://localhost/laboratuar/public/image/jLrx6DAc9LF6FfrKsMH8QNul11b4CwbcAuDZyZDW.jpeg',
                'created_at' => '2019-11-21 21:48:35',
                'updated_at' => '2019-11-21 21:48:35',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'main_title' => 'RADYO REKLAMLARI',
                'slug' => 'radyo-reklamlari',
                'title' => 'RADYO REKLAMLARI',
            'desciription' => 'Radyo güçlü iletişim mecrası olma yolunda etkisini hiçbir zaman yitirmemiştir. Radyo dinleyicilerinin %90 ından fazlası radyoyu müzik dinlemek için tercih etmektedir. Bu sebeple reklam veren radyo reklamlarında müzikal akışı bozmamak adına reklamlarını (jingle), marka veya ürünleri için hazırladıkları özel şarkı ile duyurmayı tercih etmektedirler.

Radyo dinleyicisine saygının ve akılda kalıcılığın olmazsa olmazı marka veya ürün adına yapılan özel şarkıdır. Laboratuar olarak, müşterimiz olan onlarca ulusal reklam veren firmalara özel jingle çalışmaları yaptık ve yapmaktayız.

Radyoların birçoğunun iletişim ajansı olarak, radyo kanallarının kampanya ve duyurularını da gerçekleştirmekteyiz.',
                'file' => 'http://localhost/laboratuar/public/image/hV8eucdgCAXtScsP08TGudPibiJyPUVBC2RDclad.jpeg',
                'created_at' => '2019-11-21 21:49:02',
                'updated_at' => '2019-11-21 21:49:02',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'main_title' => 'AÇIK HAVA REKLAMLAR',
                'slug' => 'acik-hava-reklamlar',
                'title' => 'AÇIK HAVA REKLAMLAR',
                'desciription' => 'Açıkhava reklamcılığı her yerde, her duvarda, her durakta. Mecra olarak akılda kalıcılık ve kampanya bütünlüğü sağlayan Açıkhava reklam alanları da dijital ekran dönüşümüyle hızla değişim sağlamaktadır.

Açıkhava reklamları ile reklam veren eksenini dijital ekrana doğru kaydırmakta ve reklam veren sabit görsellik yerine, reklam filmlerini yayınlatarak kampanya bütünlüğü sağlamaktadır.

Açıkhava reklamları çeşitlerinden olan duvar ve cephe giydirme uygulamaları yüksek ses getiren reklam yayınlarındandır.',
                'file' => 'http://localhost/laboratuar/public/image/nJdsP06waEftSIsDffOm3XrSII6MRNl16WCXy8vO.jpeg',
                'created_at' => '2019-11-21 21:49:21',
                'updated_at' => '2019-11-21 21:49:21',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'main_title' => 'DİJİTAL REKLAMCILIK',
                'slug' => 'dijital-reklamcilik',
                'title' => 'DİJİTAL REKLAMCILIK',
                'desciription' => 'Dijital reklam dünyası her geçen yıl reklam pastasından aldığı payı arttırarak, bu pastanın aslan payını alma yolunda hızla ilerlemektedir. Birçok ülkede televizyon kanallarına ayrılan bütçenin üzerinde bütçe alan dijital dünya, e-ticaret hacmini arttırarak perakende sektörünün can damarı olmaktadır.

Dijital medya reklamcılığın en önemli ve etkili mecrası uzak ara Google .

Google, arama motoru optimizasyonları, programatik, adwords reklamcılığı ve daha onlarca reklam içeriği ile tüm dijital dünyanın hakimi konumundadır.',
                'file' => 'http://localhost/laboratuar/public/image/HPTMEfqfvBI8rEdutzFUGe2mf9kDE6pUlaC3z5Ho.jpeg',
                'created_at' => '2019-11-21 21:49:41',
                'updated_at' => '2019-11-21 21:49:41',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'main_title' => 'FİKİR VE PRODÜKSİYON',
                'slug' => 'fikir-ve-produksiyon',
                'title' => 'FİKİR VE PRODÜKSİYON',
                'desciription' => 'Laboratuar Mind Craft, Fikir Sanatları Laboratuar’ı sadece medya planlama ve satın alma hizmeti sunmakla kalmıyor; adı üzerinde olan ajansımız fikir sanat ve prodüksiyon hizmetleri ile neredeyse tüm medya planlama ve satın alma hizmeti verdiği müşterilerine senaryo, reklam filmi, prodüksiyon hizmetlerinin yanı sıra masaüstü animasyon çalışmaları ile de hızlı ve etkili çalışmalar hazırlamaktadır.

Sunduğumuz fikirler ve uygulanacak prodüksiyonlar, mecralara özel olarak hazırlanmakta ve içerikleri özgün tasarımlarla kurgulanmaktadır.

Kendi stüdyolarımızın ve ekipmanlarımızın olması, hızlı ve ekonomik olarak işlem yapmamıza olanak sağlamaktadır.',
                'file' => 'http://localhost/laboratuar/public/image/pUgElLkXVpFN9Lszu8i5WnSZHTLotywhOXdyDPDe.jpeg',
                'created_at' => '2019-11-21 21:50:24',
                'updated_at' => '2019-11-21 21:50:24',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'main_title' => 'HİPERSONİK / HOLOSONİK SES SİTEMLERİ',
                'slug' => 'hipersonik-holosonik-ses-sistemleri',
                'title' => 'HİPERSONİK / HOLOSONİK SES SİTEMLERİ',
                'desciription' => 'Hipersonik Ses Sistemi dar ve sınırlandırılmış bir ses ışını yaratarak, aynı ışık gibi sesin kontrol edilmesini sağlar. Amaç; sesin sadece düz ve ince kolonun yöneldiği yönde yayılması ve diğer yerlerde gürültü oluşturmamasıdır. Hipersonik Ses sistemleri farklı sektörlerde kullanılabilir. Müzelerden sergilere, galerilerden perakende satış noktalarına ve özel projelere kadar, Dünya’nın en iyi şirketleri yüksek kalite ve hedefli ses için bu teknolojiyi seçti.',
                'file' => 'http://localhost/laboratuar/public/image/a0ljPI1fe9XIgeWpm0G3QW1mIpUrsVhwXzkyiTCv.jpeg',
                'created_at' => '2019-11-21 21:51:32',
                'updated_at' => '2019-11-21 21:51:32',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}